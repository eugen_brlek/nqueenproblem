﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EightQueenProblem.Models.ChessModel
{
    public class Queen
    {
        public int row { get; set; }
        public int col { get; set; }
    }
}
