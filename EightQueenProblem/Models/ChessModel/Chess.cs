﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EightQueenProblem.Models.ChessModel
{

    public class Chess
    {
        public int Size { get; set; }

        public List<Queen> QueenPositions;
    }
}
