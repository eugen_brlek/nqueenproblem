﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EightQueenProblem.Models.ChessModel;

namespace EightQueenProblem.Controllers
{
    public class SolveProblemController : Controller
    {

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult ViewAllSolution([FromBody]Chess chess)
        {
            if(ModelState.IsValid)
            {
                return PartialView("_ViewAllSolution", chess);
            }
            return RedirectToAction("Index");
        }
    }
}